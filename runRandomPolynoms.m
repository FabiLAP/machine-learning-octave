## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} runRandomPolynoms (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-04-23

function [retval] = runRandomPolynoms (nations, gdps)
  ## init
  args(1,1) = 0;
  args(2,1) = 0;
  args(3,1) = 0;  
  mse = intmax('int64');
  
  for i = 1:10,   
    ## make them random
    randomargs(1,1) = rand()*30+50;
    randomargs(2,1) = rand()/1000;
    randomargs(3,1) = -1*(rand()/100000000);
    
    currentMSE = getMSEFromPolynom (nations, gdps, randomargs);
    if (currentMSE<mse)
             args(1,1) = randomargs(1,1);
             args(2,1) = randomargs(2,1);
             args(3,1) = randomargs(3,1);
             mse=currentMSE;
    endif
  end;
   plotPolynom (args, 140000, nations, gdps);
   
  returnvalue = zeros(4,1);
  returnvalue(1,1)=args(1,1);
  returnvalue(2,1)=args(2,1);
  returnvalue(3,1)=args(3,1);
  returnvalue(4,1)=mse;
endfunction
