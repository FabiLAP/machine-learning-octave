## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} main (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-04-30

#evolution (nations,numberOfGenerations,numberOfChildren,parentCanSurvive,polynomComplexity)

function [retval] = main ()
    nations = dlmread("nations.csv", ",", 1, 0); 
    
%    for o = 1:5, 
%        for i = 1:5,  
%            evolution (nations,10,10,false,o);
%        end;
%    end;

%    for o = 1:5, 
%        for i = 1:5,  
%            evolution (nations,100,1,false,o);
%        end;
%    end;

%    for o = 1:5, 
%        for i = 1:5,  
%            evolution (nations,100,1,true,o);
%        end;
%    end;
    disp("######################################## CASE 4");
    for o = 1:5, 
        for i = 1:5,  
            evolution (nations,3000,1,true,o);
        end;
    end;
    disp("######################################## CASE 5");
    for o = 1:5, 
        for i = 1:5,  
            evolution (nations,1000,3,false,o);
        end;
    end;
endfunction
