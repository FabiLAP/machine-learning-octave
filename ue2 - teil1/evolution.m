## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} main (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-04-30

function [retval] = evolution (nations,numberOfGenerations,numberOfChildren,parentCanSurvive,polynomComplexity)
  polynomComplexity=polynomComplexity+1;
  #calculate the gdp (per inhabitant)
  gdp=nations(:,4)';
  for i = 1:size(nations,1), 
    gdp(1,i) = ((nations(i,4)*1000000) / nations(i,7));
  end;    
  #get scaleFactor - the maximun value
  scaleFactorGdp = max(gdp);
  #scale it!
  scaledGdp = gdp / scaleFactorGdp;
  
  #analog
  lifeSpans = nations(:, 5)';
  scaleFactorLifeSpans = max(lifeSpans);
  scaledLifeSpans = lifeSpans/scaleFactorLifeSpans;  
  
  #init starting coefficients for parent
  parent = zeros(polynomComplexity,1);
  for o = 1:polynomComplexity,
      parent(o,1) = rand();
  end;
  
  #init the MSE for this parent
  mse=getMSEFromPolynom (scaledLifeSpans, scaledGdp, parent);
  
  #generation loop
  for i = 1:numberOfGenerations,  
    #child loop
    kids = zeros(polynomComplexity,numberOfChildren);
    for j = 1:numberOfChildren,
      #create child
      kid = zeros(polynomComplexity,1);
      #params loop
      for k = 1:polynomComplexity,
        # +- max of 0,05
        value = parent(k,1) + (rand()*2-1)/10;      
        #SAVE KID IN KIDS:
        kids(k,j)=value;
      end;
    end;
    #all child are generated
    #evalute them!
    bestMSEofGeneration = intmax('int64');
    indexOfBestKid = 0;
    #child loop
    for j = 1:numberOfChildren,
      #get MSE
      MSEofKid=getMSEFromPolynom (scaledLifeSpans, scaledGdp, kids(:,j));
      #check if better
      if(MSEofKid<bestMSEofGeneration)
        bestMSEofGeneration=MSEofKid;
        indexOfBestKid=j;
      endif;
    end;
    #the fittest child
    bestOne=kids(:,indexOfBestKid);
    #check if parent can survive
    if(parentCanSurvive)
      #check if parent is better the the best kid
      if(getMSEFromPolynom(scaledLifeSpans, scaledGdp, parent) < bestMSEofGeneration)
        bestOne=parent;
      endif;
    endif;
    #found the best one! 
    parent=bestOne;
    end; 
    
  #renormalise!
  for k = 1:polynomComplexity,
    parent(k,1) = parent(k,1)*(scaleFactorLifeSpans/(scaleFactorGdp^(k-1)));      
    kids(k,j)=value;
  end;
  #show
  mse = getMSEFromPolynom(lifeSpans, gdp, parent);
  disp("MSE: ");
  disp(mse);
  disp("Coefficients");
  disp(parent);
  #plotPolynom (parent, scaleFactorGdp, lifeSpans, gdp);
  
endfunction
