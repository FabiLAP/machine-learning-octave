## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} main (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-04-30

function [retval] = mainOriginal (nations,gdps,numberOfGenerations,numberOfChildren,parentCanSurvive,polynomComplexity)
  
  #parentCanSurvive = true;
  #numberOfGenerations = 3000;
  #numberOfChildren = 1;
  #polynomComplexity = 4;
  
  scaleFactor=80/140000;
  
  #init starting coefficients
  parent = zeros(polynomComplexity,1);
  
  mse=getMSEFromPolynom (nations, gdps, parent);
  #Skylierungskoeffizienten errechnen //TODO Wherever
  
  #Schleife �ber i Generationen
  for i = 1:numberOfGenerations,  
    #Schleife �ber alle neuen Kinder 
    kids = zeros(polynomComplexity,numberOfChildren);
    for j = 1:numberOfChildren,
      #kind erzeugen
      kid = zeros(polynomComplexity,1);
      #params kid
      for k = 1:polynomComplexity,
        value = parent(k,1) + (rand()*2-1)*(80/(140000^(k-1)))/10;      
        #SAVE KID IN KIDS:
        kids(k,j)=value;
      end;
    end;
    # Kinder fertig generiert!
    # Auswertung
    bestMSEofGeneration = intmax('int64');
    indexOfBestKid = 0;
    #f�r alle Kinder
    for j = 1:numberOfChildren,
      MSEofKid=getMSEFromPolynom (nations, gdps, kids(:,j));
      if(MSEofKid<bestMSEofGeneration)
        bestMSEofGeneration=MSEofKid;
        indexOfBestKid=j;
      endif;
    end;
    #disp(indexOfBestKid);
    bestOne=kids(:,indexOfBestKid);
    #Check if Parent can survive
    if(parentCanSurvive)
      if(getMSEFromPolynom(nations, gdps, parent) < bestMSEofGeneration)
        bestOne=parent;
        #disp("took parent");
        #disp(i);
      endif;
    endif;
    #Found The best one! 
    parent=bestOne;
    #disp("bestone: ");
    #disp(bestOne);
    mse=bestMSEofGeneration;
  end;  
  disp("MSE: ");
  disp(mse);
  disp("Coefficients");
  disp(parent);
  plotPolynom (parent, 140000, nations, gdps);
  
endfunction

    ##neue kinder erzeugen mit varianz (m anzahl) -varianz mit neuen kind/10+eltern (sollte + und - sein)
    ##eltern t�ten evtl
    ##evaluieren -- siehe MSE
    ##der �berlebende wird gespeichert und zum neuen parrent.
