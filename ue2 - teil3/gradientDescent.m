## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} main (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-04-30

# Ox = die x-te Ordnung des Polynoms, A = Lernrate, E = Epochen

function [retval] = gradientDescent (nations, ordnung, lernrate, epochen)
  seconds = time();
  originalLernrate = lernrate;
  ordnung = ordnung+1;
  
  #calculate the gdp (per inhabitant)
  gdp=1000000*nations(:,4)./nations(:,7);  
  #disp(gdp);
  
  #get scaleFactor - the maximun value
  scaleFactorGdp = max(gdp);
  #disp(scaleFactorGdp);
  #scale it!
  scaledGdp = gdp / scaleFactorGdp;
  #disp(scaledGdp);
  
  #analog
  lifeSpans = nations(:,5);
  scaleFactorLifeSpans = max(lifeSpans);
  scaledLifeSpans = lifeSpans/scaleFactorLifeSpans;    
  #disp(scaledLifeSpans);
  
  unscaledgdpMatrix = repmat(gdp, 1, ordnung); #pushing scaledGDPs in Matrix, same columns
  identtity = eye(ordnung);
  coeffsVec = (tril(repmat(1, ordnung, ordnung)) * ones(ordnung, 1) - 1)'; 
  unscaledMegaGDPMatrix = unscaledgdpMatrix .^ coeffsVec;  
  #disp(unscaledMegaGDPMatrix);
  
  gdpMatrix = repmat(scaledGdp, 1, ordnung); #pushing scaledGDPs in Matrix, same columns
  #disp(gdpMatrix);
  identtity = eye(ordnung);
  coeffsVec = (tril(repmat(1, ordnung, ordnung)) * ones(ordnung, 1) - 1)'; 
  %disp(coeffsVec);
  megaGDPMatrix = gdpMatrix .^ coeffsVec;
  
  disp(megaGDPMatrix);
  #disp(' ');
  #disp(scaledLifeSpans);
  
  parent = zeros(ordnung,1); #The CoefficentsHolder  
  
  mse=getMseNoLoop (lifeSpans, megaGDPMatrix, parent);
  #mse =getMSEFromPolynom (scaledLifeSpans, scaledGdp, parent);
  
  numOfLoops=0;
  
  #epochen loop
  for e = 1:epochen, 
  #disp('NEW EPOCHE'); 
    sumOfDeltaThethas = abs(lernrate*(1/size(scaledLifeSpans,1))*(megaGDPMatrix'*((megaGDPMatrix*parent)-scaledLifeSpans)));   
    parent=parent-lernrate*(1/size(scaledLifeSpans,1))*(megaGDPMatrix'*((megaGDPMatrix*parent)-scaledLifeSpans));    
    currentmse= getMseNoLoop (lifeSpans, megaGDPMatrix, parent);
    disp('parent');
    disp(parent);
   
    #currentmse=getMSEFromPolynom (scaledLifeSpans, scaledGdp, parent);
    #evaluate mse --> break
%    if(currentmse>mse)
%        break;
%    else
%        mse = currentmse;
%    endif;
    
    #evaluate and adjust lernrate
%    if(sumOfDeltaThethas<0.01)
%        lernrate*=2;
%    else
%        lernrate=originalLernrate;
%    endif;
    numOfLoops++;

  end; 
    
  #renormalise!
  vec123s = (tril(repmat(1, ordnung, ordnung)) * ones(ordnung, 1) - 1); #[0 1 2 3 ...]
  #disp(vec123s);
  parent=parent.*(repmat(scaleFactorLifeSpans,ordnung,1)./(repmat(scaleFactorGdp,ordnung,1).^vec123s));
  #disp(parent);
  
  #mse =getMSEFromPolynom (lifeSpans, gdp, parent);
  mse = getMseNoLoop (lifeSpans, unscaledMegaGDPMatrix, parent);
  #disp("MSE: ");
  #disp(mse);
  disp("Coefficients");
  disp(parent);
  #plotPolynom (parent, scaleFactorGdp, lifeSpans', gdp');
  disp('Time of Calculation:');
  disp(time()-seconds);
  #disp('Num Of Loops');
  #disp(numOfLoops);
endfunction
