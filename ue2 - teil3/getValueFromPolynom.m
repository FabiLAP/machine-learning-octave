## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} getValueFromPolynom (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-04-23

function [retval] = getValueFromPolynom (coefficents, x)
vec123s = (tril(repmat(1, size(coefficents,1), size(coefficents,1))) * ones(size(coefficents,1), 1) - 1)'; #[0 1 2 3 ...]
adjustedx = repmat(x,size(coefficents,1),1).^(vec123s');
retval=coefficents'*adjustedx;
endfunction
