## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} runTheMainBla (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-05-01

function [retval] = runTheMainBla (nations, gdps)
disp("main (nations,gdps,3000,1,true,1);");
 main (nations,gdps,3000,1,true,1);
 main (nations,gdps,3000,1,true,1);
 main (nations,gdps,3000,1,true,1);
 main (nations,gdps,3000,1,true,1);
 main (nations,gdps,3000,1,true,1);
 
 disp("main (nations,gdps,3000,1,true,2);");
 main (nations,gdps,3000,1,true,2);
 main (nations,gdps,3000,1,true,2);
 main (nations,gdps,3000,1,true,2);
 main (nations,gdps,3000,1,true,2);
 main (nations,gdps,3000,1,true,2);
 
 disp("main (nations,gdps,3000,1,true,3);");
 main (nations,gdps,3000,1,true,3);
 main (nations,gdps,3000,1,true,3);
 main (nations,gdps,3000,1,true,3);
 main (nations,gdps,3000,1,true,3);
 main (nations,gdps,3000,1,true,3);
 
 disp("main (nations,gdps,3000,1,true,4);");
 main (nations,gdps,3000,1,true,4);
 main (nations,gdps,3000,1,true,4);
 main (nations,gdps,3000,1,true,4);
 main (nations,gdps,3000,1,true,4);
 main (nations,gdps,3000,1,true,4);
 
 disp("main (nations,gdps,3000,1,true,5);");
 main (nations,gdps,3000,1,true,5);
 main (nations,gdps,3000,1,true,5);
 main (nations,gdps,3000,1,true,5);
 main (nations,gdps,3000,1,true,5);
 main (nations,gdps,3000,1,true,5);
 
 disp("main (nations,gdps,1000,3,false,1);");
 main (nations,gdps,1000,3,false,1);
 main (nations,gdps,1000,3,false,1);
 main (nations,gdps,1000,3,false,1);
 main (nations,gdps,1000,3,false,1);
 main (nations,gdps,1000,3,false,1);
 
  disp("main (nations,gdps,1000,3,false,2);");
 main (nations,gdps,1000,3,false,2);
 main (nations,gdps,1000,3,false,2);
 main (nations,gdps,1000,3,false,2);
 main (nations,gdps,1000,3,false,2);
 main (nations,gdps,1000,3,false,2);
 
  disp("main (nations,gdps,1000,3,false,3);");
 main (nations,gdps,1000,3,false,3);
 main (nations,gdps,1000,3,false,3);
 main (nations,gdps,1000,3,false,3);
 main (nations,gdps,1000,3,false,3);
 main (nations,gdps,1000,3,false,3);
 
  disp("main (nations,gdps,1000,3,false,4);");
 main (nations,gdps,1000,3,false,4);
 main (nations,gdps,1000,3,false,4);
 main (nations,gdps,1000,3,false,4);
 main (nations,gdps,1000,3,false,4);
 main (nations,gdps,1000,3,false,4);
 
  disp("main (nations,gdps,1000,3,false,5);");
 main (nations,gdps,1000,3,false,5);
 main (nations,gdps,1000,3,false,5);
 main (nations,gdps,1000,3,false,5);
 main (nations,gdps,1000,3,false,5);
 main (nations,gdps,1000,3,false,5);

endfunction
