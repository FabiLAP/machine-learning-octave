## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} main (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-04-30

# Ox = die x-te Ordnung des Polynoms, A = Lernrate, E = Epochen

function [retval] = gradientDescent (nations, ordnung, lernrate, epochen)
  seconds = time();
  originalLernrate = lernrate;
  ordnung = ordnung+1;
  #calculate the gdp (per inhabitant)
  gdp=nations(:,4)';
  for i = 1:size(nations,1), 
    gdp(1,i) = ((nations(i,4)*1000000) / nations(i,7));
  end;    
  #get scaleFactor - the maximun value
  scaleFactorGdp = max(gdp);
  #scale it!
  scaledGdp = gdp / scaleFactorGdp;
  
  #analog
  lifeSpans = nations(:, 5)';
  scaleFactorLifeSpans = max(lifeSpans);
  scaledLifeSpans = lifeSpans/scaleFactorLifeSpans;  
  
  #init starting coefficients for parent
  parent = zeros(ordnung,1);
%  for o = 1:ordnung,
%      parent(o,1) = rand();
%  end;
  mse = getMSEFromPolynom (scaledLifeSpans, scaledGdp, parent);
  
  #init a gradientArray for saving the steps
  sumOfThetasArray = zeros(epochen,1);
  #init a msesArray for saving the steps
  mses = zeros(epochen,1);  
  
  #epochen loop
  for e = 1:epochen,  
      kid = zeros(ordnung,1);
      sumOfThetas = 0;
      #params loop
      for k = 1:ordnung,
          dTheta=0;
          for i = 1:size(scaledLifeSpans,2), 
              trueValue=scaledLifeSpans(i);
              calculatedValue=getValueFromPolynom(parent, scaledGdp(i));
              dTheta = dTheta + (calculatedValue-trueValue)*scaledGdp(i)^(k-1);
          end;
          gradient =  dTheta / size(scaledLifeSpans,2);          
          kid(k,1)=parent(k,1) - lernrate * gradient;  
          sumOfThetas += abs(lernrate * gradient);
      end;
      #new kid done
      sumOfThetasArray(e,1) = sumOfThetas;
      currentMSE = getMSEFromPolynom(scaledLifeSpans, scaledGdp, kid);
      mses(e,1)=currentMSE;
      
      #evaluate mse --> break
      if(currentMSE>mse)
          break;
      else
          mse = currentMSE;
      endif;
      
      #evaluate and adjust lernrate
      if(sumOfThetas<0.01)
          lernrate*=2;
      else
          lernrate=originalLernrate;
      endif;
      parent=kid;      
  end; 
    
  #renormalise!
  for k = 1:ordnung,
      parent(k,1) = parent(k,1)*(scaleFactorLifeSpans/(scaleFactorGdp^(k-1)));      
  end;
  
  #show
  #mse = getMSEFromPolynom(lifeSpans, gdp, parent);
  #disp("MSE: ");
  #disp(mse);
  #disp("Coefficients");
  #disp(parent);
  #plotPolynom (parent, scaleFactorGdp, lifeSpans, gdp);
  #plot(sumOfThetasArray);
  #plot(mses);
  disp('Time of Calculation:');
  disp(time()-seconds);
endfunction
