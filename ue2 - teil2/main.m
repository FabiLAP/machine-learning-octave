## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} main (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn 

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-04-30
# Ox = die x-te Ordnung des Polynoms, A = Lernrate, E = Epochen

function [retval] = main ()

nations = dlmread("nations.csv", ",", 1, 0); 
#ordnung = 5; 
#lernrate = 0.7; 
#epochen = 100;
#gradientDescent (nations, ordnung, lernrate, epochen);

disp("gradientDescent f�r Polynome 1. Grades");
gradientDescent (nations, 1, 0.01, 100);
gradientDescent (nations, 1, 0.1, 100);
gradientDescent (nations, 1, 1, 100);
gradientDescent (nations, 1, 0.01, 1000);
gradientDescent (nations, 1, 0.1, 1000);
gradientDescent (nations, 1, 1, 1000);


disp("gradientDescent f�r Polynome 3. Grades");
gradientDescent (nations, 3, 0.01, 100);
gradientDescent (nations, 3, 0.1, 100);
gradientDescent (nations, 3, 1, 100);
gradientDescent (nations, 3, 0.01, 1000);
gradientDescent (nations, 3, 0.1, 1000);
gradientDescent (nations, 3, 1, 1000);

disp("gradientDescent f�r Polynome 10. Grades");
gradientDescent (nations, 10, 0.01, 100);
gradientDescent (nations, 10, 0.1, 100);
gradientDescent (nations, 10, 1, 100);
gradientDescent (nations, 10, 0.01, 1000);
gradientDescent (nations, 10, 0.1, 1000);
gradientDescent (nations, 10, 1, 1000);

endfunction
