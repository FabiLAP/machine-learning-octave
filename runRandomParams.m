## Copyright (C) 2015 fabi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} runRandomParams (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: fabi <fabi@FABI_ULTRABOOK>
## Created: 2015-04-17

function [returnvalue] = runRandomParams (matrix, gdps)
slope = 0,
offset = 0,
mse = intmax('int64'),
for i = 1:1000, 
  randomSlope=(rand()/1000),
  randomOffset=((rand()*50)+40),
  currentMSE = getMSEwithParameter (matrix, gdps, randomSlope, randomOffset),
  if (currentMSE<mse)
         slope=randomSlope,
         offset=randomOffset,
         mse=currentMSE,
  endif
end;
returnvalue = zeros(3,1),
returnvalue(1,1)=slope,
returnvalue(2,1)=offset,
returnvalue(3,1)=mse,
endfunction
